#!/usr/bin/env python
#####by###OGUN ADEBALI###########
####UNIVERSITY OF TENNESSEE######
#Last Update: 28 November 2013
#################################
import os
import sys
import datetime
import re
import math
from glob import glob
import codostlib as codost
timestart=datetime.datetime.now()
arg=[]
arg = sys.argv
help = "\n__________\nThis script takes fasta file as an input, runs pfamscan(HMMER3) and/or hhpred, produces a table for each sequence in your fasta"
help = help+"\n\nusage: CoDoST.py -i yourfastafile.fa -d database_Probabilitycutoff_gapcutoffANDseconddatabase_secondprobability_secondgap\noptional: \n-o output.html"
help = help+"\n\nPS: In your fasta file first 50 characters of each header must be unique!\n"
help = help+"\n\nExample: CoDoST.py -i myfasta.fa -d pfam_90_50ANDcdd_85_30ANDpdb_95_25\n"
help = help+"\n\nOR: CoDoST.py -i myfasta.fa -d pfam_ANDcddANDpdb (default values are 90_50 when not defined)\n"

#Leonidas
# pathto['pfam_scan']='/home/ogun/scripts/PfamScan/pfam_scan.pl'
# pathto['pfam_scandb']='/home/ogun/allDB/Pfam27.0'
# pathto['hhsearchdb']='/home/ogun/allDB/hhpredDB/hhsearch_dbs/'
# pathto['hhsearch']='/home/ogun/tools_o/hhpred/hhsuite-2.0.16/bin/hhsearch'
# pathto['table2domfigure']='/home/ogun/scripts/table2domfigure14.py'
#god
# pathto['pfam_scan']='/home/oadebali/codost/scripts/PfamScan/pfam_scan.pl'
# pathto['pfam_scandb']='/home/oadebali/codost/db'
# pathto['hhsearchdb']='/home/oadebali/codost/db/'
# pathto['hhsearch']='/home/oadebali/codost/hhpred/hhsuite-2.0.16/bin/hhsearch'
# pathto['table2domfigure']='/home/oadebali/codost/scripts/table2domfigure151.py'
# pathto['phobius']='/home/oadebali/codost/scripts/phobius/phobius.pl'
# pathto['tmhmm']='/home/oadebali/codost/scripts/TMHMM2/tmhmm-2.0c/bin/tmhmm'



if '-h' in sys.argv:
		print "HELP INFO!"+help+"\n"
		sys.exit()

if '-i' in sys.argv:
		inputfile=sys.argv[sys.argv.index('-i')+1]
		theOrder = inputfile.split("_")[1].split(".")[0]
		DIC, LIST = codost.fastareader(inputfile)
else:
		print "No input\n"
		sys.exit()

if '-d' in sys.argv:
	parameters=sys.argv[sys.argv.index('-d')+1]	
	dbinputtext=parameters
elif '--default' not in sys.argv:
	print "No database information eg: -d pfamANDcdd\nBut that is OK. I will run HMMER only but not HHPRED"
	dbinputtext="none"

if '-b' in sys.argv:
	hhblitsparameters=sys.argv[sys.argv.index('-b')+1]	
	hhblitsdb=hhblitsparameters.split('_')[0]
	if (hhblitsdb == 'nr') or (hhblitsdb == 'uniprot') or (hhblitsdb == 'uniprotANDnr') or (hhblitsdb == 'nrANDuniprot') or (hhblitsdb == 'both'):
		try:
			hhblitsProbabilityCutoff=float(hhblitsparameters.split('_')[1])
		except:
			hhblitsProbabilityCutoff=70
		hhblits=1
	elif hhblitsdb=='None':
		hhblits=0
	else:
		print("Wrong hhblits database entered! HHblits will be ignored.")
		hhblits=0
else:
	hhblits=0
	
	
if '-o' in sys.argv:
    outputfile =sys.argv[sys.argv.index('-o')+1]
else:
    outputfile = inputfile[:(inputfile.rindex('.')-len(inputfile))]+'.html'
	
if '--cont' in sys.argv:
    cont=1
else:
    cont=0

if '--searchonce' in sys.argv:
    searchonce=1
else:
    searchonce=0
	
if '--print' in sys.argv:
	printsign=1
else:
	printsign=0
	cprintfile=open('cprint.txt','w')
	
if '--nohmmer' in sys.argv:
	runhmmer=0
else:
	runhmmer=1
	
if '--noTM' in sys.argv:
	runTM=0
else:
	runTM=1

if '-tm' in sys.argv:
	TMmethod=sys.argv[sys.argv.index('-tm')+1]
else:
	TMmethod="tmhmm"
	
if '-cpu' in sys.argv:
	cpu_number=sys.argv[sys.argv.index('-cpu')+1]
else:
	cpu_number='1'

if '-newton' in sys.argv:
	newton=1
	ID=sys.argv[sys.argv.index('-newton')+1]
	codost_paths=open("/home/oadebali/codost/scripts/codost_paths.txt",'r')
else:
	newton=0
	codost_paths=open("/home/ogun/scripts/codost_paths.txt",'r')

if '-splitdomain' in sys.argv:
	splitdomain=1
	splitpercentage=sys.argv[sys.argv.index('-splitdomain')+1]
else:
	splitdomain=0
	splitpercentage=100

if '-rpsblast' in sys.argv:
	rpsblast = 1
	rpsParamList = sys.argv[sys.argv.index('-rpsblast')+1].split('_')
	rpsDB = rpsParamList[0]
	rpsEvalue = float(rpsParamList[1])
	rpsGap = float(rpsParamList[2])
	
else:
	rpsblast = 0

if '--default' in sys.argv:
	default=1
	dbinputtext="pfam_60_30ANDcdd_60_30ANDpdb_60_30"
else:
	default=0

lines=codost_paths.readlines()
pathto={}
for line in lines:
	if not line.startswith('#'):
		pathto[line.split(':')[0]]=line.split(':')[1].replace("\n","")

hhblitsIntervals={}
		

#Dictionary to find the database with keys
fulldbname= {"custompfam"  :"custom.hhm",
			 "cdd"     :"cd",
			 "pdb"     :"pdb",
			 "cath"    :"pdb",
			 "cog"     :"COG",
			 "kog"     :"KOG",
			 "panther" :"panther",
			 "pfam"    :"pfam",
			 "pirsf"   :"pirsf",
			 "scop"    :"SCOP",
			 "smart"   :"smart",
			 "supfam"  :"supfam",
			 "tigr"    :"tigrfam",
			 "nr"	   :"nr20_12Aug11",
			 "uniprot" :"uniprot20_2015_06",
			 "Cdd"     :"cd",
			 "Cog"     :"COG",
			 "none"    :"none",
			 "None"    :"none"}

def handleWhiteSpace(text):
        thelist = text.split(' ')
        newlist = []
        for element in thelist:
                if element.strip() != '':
                        newlist.append(element)
        return newlist

		
def cprint(thetexttobeprinted):
	if printsign:
		print thetexttobeprinted
		return
	else:
		cprintfile.write(str(thetexttobeprinted))
		cprintfile.write('\n')
		return
		
def fa2nulltable(proteinheader,fullsequence):
	print "fa2nulltable"
	outputtablename=codost.removesymbols(proteinheader)[0:50]+'.table' #this is the table of the fullsequence and found domains
	if newton:
		outputtablename=ID+outputtablename
	proteinlength=len(fullsequence)
	outfile=open(outputtablename,'w')
	
	outfile.write(proteinheader + '\n') #standard table format starts with header
	outfile.write(fullsequence + '\n') #standard table has the sequence at the 2nd line
	outfile.write('No TM information\n') #tm prediction
	#outfile.write('No seg information\n') #seg
	outfile.write('[]\n') #seg
	outfile.write('[]\n') #seg
	#outfile.write('No coils information\n') #coils
	text="XXXX\t0-"+str(proteinlength)+" \tNA\tNA\tNA\tNA\tNA"
	outfile.write(text+"\n") #write domain(or gap) information
	outfile.close()
	
		
def addTMtotable(table,TMmethod):

	lines=open(table,'r').readlines()
	TM_name=table[:-5]+"fa"
	TM_out=table[:-6]+"_out.txt"
	temp=open(TM_name,'w')
	w='>'+lines[0]+'\n'+lines[1]
	temp.write(w)
	
	temp.close()
	
	def getSeg(fileName):
		outFile = fileName + '.seg'
		code = pathto['seg'] + ' ' + fileName + ' -x | ' + pathto['segParser'] + ' -i ' + fileName + ' >' + outFile
		os.system(code)
		resultLine = open(outFile,'r').readline()
		if resultLine:
			seg = resultLine.split('\t')[1].strip()
		else:
			seg = "[]"
		removefile(outFile)	
		return seg
        def getCoils(fileName):
                outFile = fileName + '.coils'
                code = pathto['coils'] + ' -f <' + fileName + ' | ' + pathto['coilsParser'] + ' -i ' + fileName + ' >' + outFile
                #print("Running coils")
		os.system(code)

		resultLine = open(outFile,'r').readline()
		
		if resultLine:
			coils = resultLine.split('\t')[1].strip()
		else:
			coils = "[]"
		removefile(outFile)	
		return coils

	if "phobius" in TMmethod:
		code="perl "+pathto['phobius']+" -short "+TM_name+" >"+TM_out
		os.system(code)
		outline=open(TM_out,'r').readlines()[1]
		outline=' '.join(outline.split())
		templist=outline.split(' ')
		del templist[0]
		newoutline='_'.join(templist)+'\n'
		del lines[2]
	elif 'tmhmm' in TMmethod:
		code=pathto['tmhmm']+" -short "+TM_name+" >"+TM_out
		os.system(code)
		outline=open(TM_out,'r').readlines()[0].replace("\n","")
		predhel=outline.split('\t')[4].split('=')[1].replace("\n","")
		topology=outline.split('\t')[5].split('=')[1].replace("\n","")
		newoutline=predhel+'_'+'0'+'_'+topology+'\n'
		del lines[2]

	else:
		cprint("no correct TM prediction method is stated")
		#return 0

		
	lines.insert(2,newoutline)
	#lines.insert(3,getSeg(TM_name) + "\n")
	lines[3]= getSeg(TM_name) + "\n"
	#lines.insert(4,getCoils(TM_name) + "\n")
	lines[4] = getCoils(TM_name) + "\n"
	outfile=open(table,'w')
	for line in lines:
		outfile.write(line)
	outfile.close()
	removefile(TM_name)
	removefile(TM_out)
		
def table2pfamscantable(table):
#HMMER3 search on Pfam27.0
#It takes only 1 sequence in fasta format eg temp.fa	

	tablefile=open(table,'r')
	lines=tablefile.readlines()
	header=lines[0].strip()
	outputtablename=table #this is the table of the sequence and found domains
	fullsequence=lines[1].strip()
	proteinlength=len(fullsequence)
	TMline=lines[2].strip()
	segLine=lines[3].strip()
	coilsLine=lines[4].strip()
	cprint(TMline)
	pscanfa_name=table[:-5]+'fa'
	
	temp0=open(pscanfa_name,'w')
	text=">"+header+"\n"+fullsequence+"\n"
	temp0.write(text)
	temp0.close()
	pscan_out=pscanfa_name[:-2]+'txt'
	
	code="perl "+pathto['pfam_scan']+" -align -cpu "+cpu_number+" -outfile "+pscan_out+" -fasta "+pscanfa_name+" -d "+pathto['pfam_scandb'] #Run pfam_scan! It is highly specific!
	os.system(code)
	removefile(pscanfa_name)
	pscan=open(pscan_out,'r') #It is time to parse the results
	lines=pscan.readlines()
	pscan.close()
	#removefile(pscan_out)
	dbname="Pfam28.0" #Just a name of databse to know where the domain comes from
	
	intervallist=[]
	domainslist=[]
	dblist=[]
	scorelist=[]
	alignmentlist=[]
	sequencelist=[]
	
	locations='0-'
	domaintext='XXXX;'
	domscore='XXXX;'
	domdb='XXXX;'
	templateSELCtext='XXXX;'
	alignmenttext='XXXX;'
	sequencetext='XXXX;'

	
	for line in lines:
		line=line.replace("\n","")
		if not line is "":
			if not line.startswith("#"):
				seqline=lines[lines.index(line+"\n")+4][11:].replace("\n","").replace(" ","_")
				alignment=lines[lines.index(line+"\n")+2][11:].replace("\n","").replace(" ","_")
				line=line.replace("\n","")
				HMMfeaturesList = [x for x in line.split(" ") if x]
				if len(HMMfeaturesList)>13:
					domname=HMMfeaturesList[6]
				else:
					domname=HMMfeaturesList[5]

				#domstart=[x for x in line.split(" ") if x][3]
				#domend=str(int([x for x in line.split(" ") if x][4])-1)
				score=HMMfeaturesList[-4]
				evalue=HMMfeaturesList[-3]
				pfam_scan_score = score + "/" + evalue 
				aliS = domstart = HMMfeaturesList[1]
				aliE = domend =HMMfeaturesList[2]
				templateS=HMMfeaturesList[-7]
				templateE=HMMfeaturesList[-6]
				templateL=HMMfeaturesList[-5]
				if templateL=="0":
					templateL = templateE
				templateC=(float(templateE)-float(templateS))/max(int(templateE),int(templateL))
				domaintext=domaintext+domname+";XXXX;"
				locations=locations+domstart+";"+domstart+"-"+domend+";"+domend+"-"
				domscore=domscore+pfam_scan_score+";XXXX;"
				domdb=domdb+dbname+";XXXX;"
				templateSELCtext=templateSELCtext+aliS+'-'+aliE+'|'+templateS+'-'+templateE+'|'+templateL+'|'+str(round(templateC,2))+";XXXX;"
				alignmenttext=alignmenttext+alignment+";XXXX;"
				sequencetext=sequencetext+seqline+";XXXX;"
				# print seqline
				# print alignment
				
				
	locations=locations+str(proteinlength)+';'

	#code="rm "+outputtablename
	#os.system(code)
	outfile=open(outputtablename,'w')
	
	outfile.write(header + '\n') #standard table format starts with header
	outfile.write(fullsequence + '\n') #standard table has the sequence at the 2nd line
	outfile.write(TMline + '\n') #standard table has the TM information at the 3rd line
	outfile.write(segLine + '\n') #standard table has the TM information at the 3rd line
	outfile.write(coilsLine + '\n') #standard table has the TM information at the 3rd line
	
	for i in range(0,len(domaintext.split(";"))-1):
		# text=str(domaintext.split(";")[i])+'\t'+str(locations.split(";")[i])+'\t'+str(domscore.split(";")[i])+'\t'+str(domdb.split(";")[i])+'\t'+str(templateSELCtext.split(";")[i]+'\t'+str(sequencetext.split(";")[i])+'\t'+str(alignmenttext.split(";")[i])
		text=str(domaintext.split(";")[i])+'\t'+str(locations.split(";")[i])+'\t'+str(domscore.split(";")[i])+'\t'+str(domdb.split(";")[i])+'\t'+str(templateSELCtext.split(";")[i])+'\t'+str(sequencetext.split(";")[i])+'\t'+str(alignmenttext.split(";")[i])
		#cprint(text)
		outfile.write(text+"\n") #write domain(or gap) information
	
	outfile.close()
	print "HMMER3 is executed using pfamscan.pl (from PFAM)\n"
	print "________________________________________________\n"
	
	
def runhhblits(fasta,hhblitsdb):
	tmplist=fasta.split('_')
	hhblitsout=''
	for m in range(0,len(tmplist)-1):
		if (m != 0) and (m != len(tmplist)-1):
			hhblitsout=hhblitsout+'_'
		hhblitsout=hhblitsout+tmplist[m]
	hhblitsout=hhblitsout+"_"+hhblitsdb+".a3m"
	if os.path.isfile(hhblitsout):
		pass
	else:
		code=pathto['hhblits']+" -maxmem 2 -cpu "+cpu_number+" -i "+fasta+" -d "+pathto['hhblitsdb']+fulldbname[hhblitsdb]+' -oa3m '+hhblitsout
		os.system(code) #hhblits to generate MSA as query for domain detection
	return hhblitsout
					
	
def table2hhpredtable(table,database,probability_cutoff,interdomaingap_cutoff):
#This function updates the given table by searching the domains in the "gaps" of the protein sequence on the given database
	
	if (database.startswith("None")) or (database.startswith("none")):
		return
	tf=open(table,'r')
	if ('consensus' in tf.readlines()[0]):
		cprint( "The header cannot include 'consensus' word! Exiting...")
		open("detailed_output.txt",'a').write("The header cannot include 'consensus' word! Please try again removing or modifying this word")
		sys.exit()
	tf.close()
	
	#Interdomain gap cutoff decides to determine the gap. To decide if it is worth to search or not!
	#eg: if the gap length is under 30 do not search any domain in that region. 
	
	header,fullsequence,TMinfo,segInfo,coilsInfo,domainlist,intervallist,scorelist,dblist,templateSELClist,sequencelist,alignmentlist=table2lists(table) #Convert table format to lists
	
	
	
	
	newdomainlist=[]
	newintervallist=[]
	newscorelist=[]
	newdblist=[]
	newtemplateSELClist=[]
	newsequencelist=[]
	newalignmentlist=[]
	
	

	searchisgoingon= "Yes"
	roundnum=0
	searched_list=[]
	hhblits_searched_list=[]
	Founddomainnum=0
	
	while searchisgoingon is "Yes": 	#Re run the domain search on the gaps till no new domain is found!
		roundnum=roundnum+1
		searchisgoingon="No"	#This will be changed to "Yes" if a new domain is found
		
		
		#in case nothing is found, just return the original lists and keep the original table
		newdomainlist=list(domainlist)
		newintervallist=list(intervallist)
		newscorelist=list(scorelist)
		newdblist=list(dblist)
		newtemplateSELClist=list(templateSELClist)
		newsequencelist=list(sequencelist)
		newalignmentlist=list(alignmentlist)
		
		for j in range(0,len(domainlist)): #for each interval in the table
			cprint( "Start over with "+str(j))
			
			if  ("XXXX" in domainlist[j]): #if there is no domain in this interval
				
				intervalstart=int(intervallist[j].split("-")[0]) #retrieve interval start
				intervalend=int(intervallist[j].split("-")[1])	#retrieve interval end
				gaplength=intervalend-intervalstart
				
				matchsequence=''
		
				#Not to search the same interval if nothing is found before:
				uniqueintervalname=codost.removesymbols(header)[0:50]+'_'+intervallist[j].split("-")[0]+'-'+intervallist[j].split("-")[1]+'_'+database
				secondlist=[]
				
				
				if int(gaplength)>interdomaingap_cutoff and not uniqueintervalname in searched_list:
				# If the gaplength is worth to search and if it is for the first time this gap is being searched
					cprint("working on '"+uniqueintervalname+"' on the '"+database+"' database")
					#cprint('Working on the gap ---> round:'+str(roundnum)+'\t intervalnumber:'+str(j)+'\t interval:'+str(intervallist[j]))
					uniqueintervalfile=uniqueintervalname+'.fa'
					uniqueinterval_out=uniqueintervalname+'.hhr'
					
					outfile=open(uniqueintervalfile,'w') # A
					outfile.write(">"+uniqueintervalname+"\n")
					searched_list.append(uniqueintervalname)
					#outfile.write(fullsequence[int(intervallist[j].split('-')[0]):int(intervallist[j].split('-')[1])])
					outfile.write(fullsequence[int(intervallist[j].split('-')[0]):int(intervallist[j].split('-')[1])-1])
					outfile.close()
					
					if database=='tigr':
						cpu_num=1
					else:
						cpu_num=cpu_number
				
					code=pathto['hhsearch']+" -maxmem 2 -b 1 -B 1 -aliw 10000 -cpu "+str(cpu_num)+" -i "+uniqueintervalfile+" -d "+pathto['hhsearchdb']+fulldbname[database]
					os.system(code) #hhsearch single sequence as query

					hhr=open(uniqueinterval_out,'r') # Parse the results
					lines=hhr.readlines()
					
					removefile(uniqueinterval_out)
					if hhblits==0:
						removefile(uniqueintervalfile)
				
					
					the_db_name=database

					probability=0
					
					probabilitypattern=re.compile("([0-9][0-9]\.[0-9])|([1][0][0]\.[0])")
					
					linenine=lines[9].split(' ')
					linenineplus=[x for x in linenine if x != '']
					for element in linenineplus:
						element=element.strip()
						result=probabilitypattern.match(element)
						if result:
							probability=float(element)
							break
						
						
					#Check if something significant found from HHsearch by single sequence query
					if probability >= probability_cutoff:
						searchisgoingon="Yes"
						Founddomainnum=Founddomainnum+1
						
						the_score=probability
						the_score=str(the_score)
						the_original_cd_name=lines[9].split("  ")[1].split(" ")[1].replace(" ","")
						if ("pfam" in database) or ("custompfam"==database):
							the_cd_name=lines[9].split("  ")[1].split(" ")[2].replace(" ","")[:-1]

						else:
							the_cd_name=the_original_cd_name
						#Parsing the outputfile
						for line in lines:
							if line.startswith("No 1"):
								indnum=lines.index(line)
								sequence=(' '.join(lines[indnum+4].split())).split(' ')[3].replace("\n","")
								alignment=lines[indnum+6][22:].replace(" ","_").replace("\n","")
								queryline=lines[indnum+5]
								templateline=lines[indnum+7]
								templatesequenceline=lines[indnum+8]
								qinitial=intervalstart+int([x for x in queryline.split("Consensus")[1].split(" ") if x][0])
								tinitial=int([x for x in templateline.split("Consensus")[1].split(" ") if x][0])
								qend=intervalstart+int([x for x in queryline.split("Consensus")[1].split(" ") if x][-2])
								tend=int([x for x in templateline.split("Consensus")[1].split(" ") if x][-2])
								#matchsequnce=[x for x in templatesequenceline.split(the_original_cd_name)[1].split(" ") if x][1]
								the_template_interval=str(tinitial)+'-'+str(tend)
								
								the_template_interval_length=tend-tinitial
								the_hmm_length=int([x for x in templateline.split("Consensus")[1].split(" ") if x][-1].replace("(","").replace(")",""))
								the_coverage=float(the_template_interval_length)/int(the_hmm_length)
								SELC='NA-NA|'+the_template_interval+'|'+str(the_hmm_length)+'|'+str(round(the_coverage,2))
							
						n=j
						# newdomainlist=domainlist
						del newdomainlist[j]
						newdomainlist.insert(n,'XXXX')
						newdomainlist.insert(n+1,the_cd_name)
						newdomainlist.insert(n+2,'XXXX')
						
						# newintervallist=intervallist
						oldstart=newintervallist[j].split("-")[0]
						firstnewinterval=oldstart+'-'+str(qinitial)
						the_interval=str(qinitial)+'-'+str(qend)
						oldend=newintervallist[j].split("-")[1]
						thirdnewinterval=str(qend)+'-'+oldend
						
						del newintervallist[j]
						newintervallist.insert(n,firstnewinterval)
						newintervallist.insert(n+1,the_interval)
						newintervallist.insert(n+2,thirdnewinterval)
					
						# newscorelist=scorelist
						del newscorelist[j]
						newscorelist.insert(n,'XXXX')
						newscorelist.insert(n+1,the_score)
						newscorelist.insert(n+2,'XXXX')
						
						# newdblist=dblist
						del newdblist[j]
						newdblist.insert(n,'XXXX')
						newdblist.insert(n+1,the_db_name)
						newdblist.insert(n+2,'XXXX')
						
						# newtemplateSELClist=templateSELClist
						del newtemplateSELClist[j]
						newtemplateSELClist.insert(n,'XXXX')
						newtemplateSELClist.insert(n+1,SELC)
						newtemplateSELClist.insert(n+2,'XXXX')
						
						# newsequencelist=sequencelist
						del newsequencelist[j]
						newsequencelist.insert(n,'XXXX')
						newsequencelist.insert(n+1,sequence)
						newsequencelist.insert(n+2,'XXXX')
						
						# newalignmentlist=alignmentlist
						del newalignmentlist[j]
						newalignmentlist.insert(n,'XXXX')
						newalignmentlist.insert(n+1,alignment)
						newalignmentlist.insert(n+2,'XXXX')
						
						for i in range(0,len(newdomainlist)):
							cprint (newdomainlist[i]+'\t'+newintervallist[i]+'\t'+str(newscorelist[i])+'\t'+newdblist[i]+'\t'+newtemplateSELClist[i]+'\t'+newsequencelist[i]+'\t'+newalignmentlist[i])

			
						#update the lists,break and startover. 
						domainlist=list(newdomainlist)
						intervallist=list(newintervallist)
						scorelist=list(newscorelist)
						dblist=list(newdblist)
						templateSELClist=list(newtemplateSELClist)
						sequencelist=list(newsequencelist)
						alignmentlist=list(newalignmentlist)
						
						if searchonce:
							searchisgoingon="No"
						break
					
					#If nothing is found by single sequence query alignment, generate MSA and run it again!
					# This interval shouldn't be searched on the same database (eg pfam) with the same query (nr)
					# elif hhblits and (hhblitsIntervals[uniqueintervalname]==0):
					elif hhblits:
						# global hhblitsdb
						if hhblitsdb=='both':
							newhhblitsdb='uniprot'
						elif hhblitsdb=='uniprotANDnr':
							newhhblitsdb='uniprot'
						elif hhblitsdb=='nrANDuniprot':
							newhhblitsdb='nr'
						elif hhblitsdb!= "nr" and hhblitsdb!="uniprot":
							cprint("hhblits database definition is not correct! CODE: 9487 Exiting!" + hhblitsdb)
						else:
							newhhblitsdb = hhblitsdb
						hhblitsuniqueintervalname=uniqueintervalname+"_"+newhhblitsdb
						if not hhblitsuniqueintervalname in hhblits_searched_list: #If this interval has been searched before we don't need to do it one more time!
							hhblits_searched_list.append(hhblitsuniqueintervalname) #Save this interval on the HHsearch database and query database in the searched list
							cprint( "Running HHblits to generate MSA on")
							cprint(newhhblitsdb)
							hhblitsout=runhhblits(uniqueintervalfile,newhhblitsdb) #Generate MSA
							hhblitsIntervals[uniqueintervalname]=1
							uniqueintervalfile_hhr=uniqueintervalname+'.hhr'
							if database=='tigr':
								cpu_num=1
							else:
								cpu_num=cpu_number
							code=pathto['hhsearch']+" -b 1 -B 1 -aliw 10000 -cpu "+str(cpu_num)+" -i "+hhblitsout+" -d "+pathto['hhsearchdb']+fulldbname[database]+' -o '+uniqueintervalfile_hhr
							os.system(code) #hhsearch multiple sequence as query
							hhblitsIntervals[uniqueintervalname]=1
							hhr=open(uniqueinterval_out,'r') # Parse the results
							lines=hhr.readlines()
							# removefile(uniqueinterval_out)
							# removefile(uniqueintervalfile)
							
							the_db_name=database

							probability=0
							
							probabilitypattern=re.compile("([0-9][0-9]\.[0-9])|([1][0][0]\.[0])")
							
							linenine=lines[9].split(' ')
							linenineplus=[x for x in linenine if x != '']
							for element in linenineplus:
								element=element.strip()
								
								result=probabilitypattern.match(element)
								if result:
									probability=float(element)
									break

							#Check if something significant found
							if probability >= hhblitsProbabilityCutoff:
								searchisgoingon="Yes"
								Founddomainnum=Founddomainnum+1
								
								the_score=probability
								the_score=str(the_score)
								the_original_cd_name=lines[9].split("  ")[1].split(" ")[1].replace(" ","")
								if "pfam" in database:
									the_cd_name=lines[9].split("  ")[1].split(" ")[2].replace(" ","")[:-1]
								else:
									the_cd_name=the_original_cd_name
								

								for line in lines:
									if line.startswith("No 1"):
										indnum=lines.index(line)
										sequence=(' '.join(lines[indnum+4].split())).split(' ')[3].replace("\n","")
										alignment=lines[indnum+6][22:].replace(" ","_").replace("\n","")
										queryline=lines[indnum+5]
										templateline=lines[indnum+7]
										templatesequenceline=lines[indnum+8]
										qinitial=intervalstart+int([x for x in queryline.split("Consensus")[1].split(" ") if x][0])
										tinitial=int([x for x in templateline.split("Consensus")[1].split(" ") if x][0])
										qend=intervalstart+int([x for x in queryline.split("Consensus")[1].split(" ") if x][-2])
										tend=int([x for x in templateline.split("Consensus")[1].split(" ") if x][-2])
										#matchsequnce=[x for x in templatesequenceline.split(the_original_cd_name)[1].split(" ") if x][1]
										the_template_interval=str(tinitial)+'-'+str(tend)
										
										the_template_interval_length=tend-tinitial
										the_hmm_length=int([x for x in templateline.split("Consensus")[1].split(" ") if x][-1].replace("(","").replace(")",""))
										the_coverage=float(the_template_interval_length)/int(the_hmm_length)
										SELC='NA-NA|'+the_template_interval+'|'+str(the_hmm_length)+'|'+str(round(the_coverage,2))
									
								n=j
								# newdomainlist=domainlist
								del newdomainlist[j]
								newdomainlist.insert(n,'XXXX')
								newdomainlist.insert(n+1,the_cd_name)
								newdomainlist.insert(n+2,'XXXX')
								
								# newintervallist=intervallist
								oldstart=newintervallist[j].split("-")[0]
								firstnewinterval=oldstart+'-'+str(qinitial)
								the_interval=str(qinitial)+'-'+str(qend)
								oldend=newintervallist[j].split("-")[1]
								thirdnewinterval=str(qend)+'-'+oldend
								
								del newintervallist[j]
								newintervallist.insert(n,firstnewinterval)
								newintervallist.insert(n+1,the_interval)
								newintervallist.insert(n+2,thirdnewinterval)
							
								# newscorelist=scorelist
								del newscorelist[j]
								newscorelist.insert(n,'XXXX')
								newscorelist.insert(n+1,the_score)
								newscorelist.insert(n+2,'XXXX')
								
								# newdblist=dblist
								del newdblist[j]
								newdblist.insert(n,'XXXX')
								newdblist.insert(n+1,the_db_name)
								newdblist.insert(n+2,'XXXX')
								
								# newtemplateSELClist=templateSELClist
								del newtemplateSELClist[j]
								newtemplateSELClist.insert(n,'XXXX')
								newtemplateSELClist.insert(n+1,SELC)
								newtemplateSELClist.insert(n+2,'XXXX')
								
								# newsequencelist=sequencelist
								del newsequencelist[j]
								newsequencelist.insert(n,'XXXX')
								newsequencelist.insert(n+1,sequence)
								newsequencelist.insert(n+2,'XXXX')
								
								# newalignmentlist=alignmentlist
								del newalignmentlist[j]
								newalignmentlist.insert(n,'XXXX')
								newalignmentlist.insert(n+1,alignment)
								newalignmentlist.insert(n+2,'XXXX')
								
								for i in range(0,len(newdomainlist)):
									cprint (newdomainlist[i]+'\t'+newintervallist[i]+'\t'+str(newscorelist[i])+'\t'+newdblist[i]+'\t'+newtemplateSELClist[i]+'\t'+newsequencelist[i]+'\t'+newalignmentlist[i])

					
								#update the lists,break and startover. 
								domainlist=list(newdomainlist)
								intervallist=list(newintervallist)
								scorelist=list(newscorelist)
								dblist=list(newdblist)
								templateSELClist=list(newtemplateSELClist)
								sequencelist=list(newsequencelist)
								alignmentlist=list(newalignmentlist)
								
								
								break
							
							elif hhblitsdb!='nr' or hhblitsdb!='uniprot': #If nothing found after the first hhblits+hhsearch try hhblits with the other database
								if hhblitsdb=='both':
									newhhblitsdb='nr'
								elif hhblitsdb=='uniprotANDnr':
									newhhblitsdb='nr'
								elif hhblitsdb=='nrANDuniprot':
									newhhblitsdb='uniprot'
								else:
									cprint("hhblits database definition is not correct! CODE: 9488 Exiting!")
								hhblitsuniqueintervalname=uniqueintervalname+"_"+newhhblitsdb
								if not hhblitsuniqueintervalname in hhblits_searched_list: #If this interval has been searched before we don't need to do it one more time!
									hhblits_searched_list.append(hhblitsuniqueintervalname) #Save this interval on the HHsearch database and query database in the searched list
									cprint( "Running HHblits to generate MSA on")
									cprint(newhhblitsdb)
									hhblitsout=runhhblits(uniqueintervalfile,newhhblitsdb) #Generate MSA
									hhblitsIntervals[uniqueintervalname]=1
									uniqueintervalfile_hhr=uniqueintervalname+'.hhr'
									if database=='tigr':
										cpu_num=1
									else:
										cpu_num=cpu_number
									code=pathto['hhsearch']+" -b 1 -B 1 -aliw 10000 -cpu "+str(cpu_num)+" -i "+hhblitsout+" -d "+pathto['hhsearchdb']+fulldbname[database]+' -o '+uniqueintervalfile_hhr
									os.system(code) #hhsearch multiple sequence as query
									hhblitsIntervals[uniqueintervalname]=1
									hhr=open(uniqueinterval_out,'r') # Parse the results
									lines=hhr.readlines()
									# removefile(uniqueinterval_out)
									# removefile(uniqueintervalfile)
									
									the_db_name=database

									probability=0
									
									probabilitypattern=re.compile("([0-9][0-9]\.[0-9])|([1][0][0]\.[0])")
									
									linenine=lines[9].split(' ')
									linenineplus=[x for x in linenine if x != '']
									for element in linenineplus:
										element=element.strip()
										
										result=probabilitypattern.match(element)
										if result:
											probability=float(element)
											break

									#Check if something significant found
									if probability >= hhblitsProbabilityCutoff:
										searchisgoingon="Yes"
										Founddomainnum=Founddomainnum+1
										
										the_score=probability
										the_score=str(the_score)
										the_original_cd_name=lines[9].split("  ")[1].split(" ")[1].replace(" ","")
										if "pfam" in database:
											the_cd_name=lines[9].split("  ")[1].split(" ")[2].replace(" ","")[:-1]
										else:
											the_cd_name=the_original_cd_name
										

										for line in lines:
											if line.startswith("No 1"):
												indnum=lines.index(line)
												sequence=(' '.join(lines[indnum+4].split())).split(' ')[3].replace("\n","")
												alignment=lines[indnum+6][22:].replace(" ","_").replace("\n","")
												queryline=lines[indnum+5]
												templateline=lines[indnum+7]
												templatesequenceline=lines[indnum+8]
												qinitial=intervalstart+int([x for x in queryline.split("Consensus")[1].split(" ") if x][0])
												tinitial=int([x for x in templateline.split("Consensus")[1].split(" ") if x][0])
												qend=intervalstart+int([x for x in queryline.split("Consensus")[1].split(" ") if x][-2])
												tend=int([x for x in templateline.split("Consensus")[1].split(" ") if x][-2])
												#matchsequnce=[x for x in templatesequenceline.split(the_original_cd_name)[1].split(" ") if x][1]
												the_template_interval=str(tinitial)+'-'+str(tend)
												
												the_template_interval_length=tend-tinitial
												the_hmm_length=int([x for x in templateline.split("Consensus")[1].split(" ") if x][-1].replace("(","").replace(")",""))
												the_coverage=float(the_template_interval_length)/int(the_hmm_length)
												SELC='NA-NA|'+the_template_interval+'|'+str(the_hmm_length)+'|'+str(round(the_coverage,2))
											
										n=j
										# newdomainlist=domainlist
										del newdomainlist[j]
										newdomainlist.insert(n,'XXXX')
										newdomainlist.insert(n+1,the_cd_name)
										newdomainlist.insert(n+2,'XXXX')
										
										# newintervallist=intervallist
										oldstart=newintervallist[j].split("-")[0]
										firstnewinterval=oldstart+'-'+str(qinitial)
										the_interval=str(qinitial)+'-'+str(qend)
										oldend=newintervallist[j].split("-")[1]
										thirdnewinterval=str(qend)+'-'+oldend
										
										del newintervallist[j]
										newintervallist.insert(n,firstnewinterval)
										newintervallist.insert(n+1,the_interval)
										newintervallist.insert(n+2,thirdnewinterval)
									
										# newscorelist=scorelist
										del newscorelist[j]
										newscorelist.insert(n,'XXXX')
										newscorelist.insert(n+1,the_score)
										newscorelist.insert(n+2,'XXXX')
										
										# newdblist=dblist
										del newdblist[j]
										newdblist.insert(n,'XXXX')
										newdblist.insert(n+1,the_db_name)
										newdblist.insert(n+2,'XXXX')
										
										# newtemplateSELClist=templateSELClist
										del newtemplateSELClist[j]
										newtemplateSELClist.insert(n,'XXXX')
										newtemplateSELClist.insert(n+1,SELC)
										newtemplateSELClist.insert(n+2,'XXXX')
										
										# newsequencelist=sequencelist
										del newsequencelist[j]
										newsequencelist.insert(n,'XXXX')
										newsequencelist.insert(n+1,sequence)
										newsequencelist.insert(n+2,'XXXX')
										
										# newalignmentlist=alignmentlist
										del newalignmentlist[j]
										newalignmentlist.insert(n,'XXXX')
										newalignmentlist.insert(n+1,alignment)
										newalignmentlist.insert(n+2,'XXXX')
										
										for i in range(0,len(newdomainlist)):
											cprint (newdomainlist[i]+'\t'+newintervallist[i]+'\t'+str(newscorelist[i])+'\t'+newdblist[i]+'\t'+newtemplateSELClist[i]+'\t'+newsequencelist[i]+'\t'+newalignmentlist[i])

							
										#update the lists,break and startover. 
										domainlist=list(newdomainlist)
										intervallist=list(newintervallist)
										scorelist=list(newscorelist)
										dblist=list(newdblist)
										templateSELClist=list(newtemplateSELClist)
										sequencelist=list(newsequencelist)
										alignmentlist=list(newalignmentlist)
										
										
										break
								
						# else:
							# pass
							
					
					
	#Write the results to the table
	outputtable=table
	outf=open(outputtable,'w')	
	outf.write(header + '\n')
	outf.write(fullsequence + '\n')
	outf.write(TMinfo + '\n')
	outf.write(segInfo + '\n')
	outf.write(coilsInfo + '\n')
	
	cprint("Found domain number")
	cprint(Founddomainnum)
	cprint("_______________________")
	cprint("End of hhpred")

	
	for i in range(0,len(domainlist)):
		text=domainlist[i]+'\t'+intervallist[i]+'\t'+scorelist[i]+'\t'+dblist[i]+'\t'+templateSELClist[i]+'\t'+sequencelist[i]+'\t'+alignmentlist[i]+'\n'
		cprint (text)
		outf.write(text)
	outf.close()
		
def table2RPStable(table,database,evalue_cutoff,interdomaingap_cutoff):
#This function updates the given table by searching the domains in the "gaps" of the protein sequence on the given database
	themethod = "RPS"	
	if (database.startswith("None")) or (database.startswith("none")):
		return


	evalue_cutoff = min(evalue_cutoff,1)	
	#Interdomain gap cutoff decides to determine the gap. To decide if it is worth to search or not!
	#eg: if the gap length is under 30 do not search any domain in that region. 
	
	header,fullsequence,TMinfo,segInfo,coilsInfo,domainlist,intervallist,scorelist,dblist,templateSELClist,sequencelist,alignmentlist=table2lists(table) #Convert table format to lists
	
	
	
	
	newdomainlist=[]
	newintervallist=[]
	newscorelist=[]
	newdblist=[]
	newtemplateSELClist=[]
	newsequencelist=[]
	newalignmentlist=[]
	
	

	searchisgoingon= "Yes"
	roundnum=0
	searched_list=[]
	hhblits_searched_list=[]
	Founddomainnum=0
	
	while searchisgoingon is "Yes": 	#Re run the domain search on the gaps till no new domain is found!
		roundnum=roundnum+1
		searchisgoingon="No"	#This will be changed to "Yes" if a new domain is found
		
		
		#in case nothing is found, just return the original lists and keep the original table
		newdomainlist=list(domainlist)
		newintervallist=list(intervallist)
		newscorelist=list(scorelist)
		newdblist=list(dblist)
		newtemplateSELClist=list(templateSELClist)
		newsequencelist=list(sequencelist)
		newalignmentlist=list(alignmentlist)
		
		for j in range(0,len(domainlist)): #for each interval in the table
			cprint( "Start over with "+str(j))
			
			if  ("XXXX" in domainlist[j]): #if there is no domain in this interval
				
				intervalstart=int(intervallist[j].split("-")[0]) #retrieve interval start
				intervalend=int(intervallist[j].split("-")[1])	#retrieve interval end
				gaplength=intervalend-intervalstart
				
				matchsequence=''
		
				#Not to search the same interval if nothing is found before:
				uniqueintervalname=codost.removesymbols(header)[0:50]+'_'+intervallist[j].split("-")[0]+'-'+intervallist[j].split("-")[1]+'_' + themethod + '_' + database
				secondlist=[]
				
				
				if int(gaplength)>interdomaingap_cutoff and not uniqueintervalname in searched_list:
				# If the gaplength is worth to search and if it is for the first time this gap is being searched
					cprint("working on '"+uniqueintervalname+"' on the '"+database+"' database")
					#cprint('Working on the gap ---> round:'+str(roundnum)+'\t intervalnumber:'+str(j)+'\t interval:'+str(intervallist[j]))
					uniqueintervalfile=uniqueintervalname+'.fa'
					uniqueinterval_out=uniqueintervalname+'.rps'
					
					outfile=open(uniqueintervalfile,'w') # A
					outfile.write(">"+uniqueintervalname+"\n")
					searched_list.append(uniqueintervalname)
					outfile.write(fullsequence[int(intervallist[j].split('-')[0]):int(intervallist[j].split('-')[1])])
					outfile.close()
					
				
					code=pathto['rpsblast']+" -query "+uniqueintervalfile+" -db "+pathto['rpsblastdb']+fulldbname[database] + " -out " + uniqueinterval_out + " -max_hsps 1 -num_alignments 1 "
					os.system(code) #hhsearch single sequence as query

					rps=open(uniqueinterval_out,'r') # Parse the results
					lines=rps.readlines()
					
					#removefile(uniqueinterval_out)
				
					the_db_name=database

					probability=0
					
					thereishit =0
					foundRPS = 0
					Expect = 10000000
					for linenum in range(0,len(lines)):
						if lines[linenum].startswith("Sequences producing significant alignments"):
							print '---->', lines[linenum+2]
							print '------->', lines[linenum+2].split(' ')
							print '------->', lines[linenum+2].split(' ')[-2]
							print '------->', lines[linenum+2].split(' ')[-3]
							Expect = float(lines[linenum + 2].strip().split(' ')[-1])
							foundRPS = 1
						if lines[linenum].startswith(">"):
							descriptionline = lines[linenum]
							cd_name = descriptionline.split(' ')[1].split(',')[0]
							l = linenum + 1
							while not lines[l].startswith("Length="):
								l += 1
							else:
								sbjctlength = int(lines[l].split('=')[1])
							break
						
					#Check if something significant found by RPSBLAST by single sequence query
					if Expect <= evalue_cutoff and foundRPS ==1:
						searchisgoingon="Yes"
						Founddomainnum=Founddomainnum+1
						
						the_score=Expect
						the_score=str(the_score)
						the_original_cd_name=cd_name
						if (cd_name.startswith("pfam")):
							the_cd_name=descriptionline.split(',')[1].strip()

						else:
							the_cd_name=the_original_cd_name

						#Parsing the outputfile


						for linenum in range(0,len(lines)):
						        if lines[linenum].startswith(" Score ="):
						                thereishit = 1
						                Scoreline = lines[linenum]
						                #Expect = float(Scoreline.split("Expect = ")[1].strip())
						                queryline = ''
						                alignmentline = ''
						                sbjctline = ''
						                qnum = linenum + 3
						                querystart = int(handleWhiteSpace(lines[qnum])[1])
						                sbjctstart = int(handleWhiteSpace(lines[qnum+2])[1])
						                while (lines[qnum].startswith('Query')):
						                        letterend = 13+len(handleWhiteSpace(lines[qnum])[2])
						                        queryline += lines[qnum][13:letterend]
						                        alignmentline += lines[qnum+1][13:letterend]
						                        sbjctline += lines[qnum+2][13:letterend]
						                        qnum += 4
						                else:
						                        queryend = int(handleWhiteSpace(lines[qnum-4])[3])
						                        sbjctend = int(handleWhiteSpace(lines[qnum-2])[3])
						
						                break



						the_template_interval = str(sbjctstart) + '-' + str(sbjctend)
						the_template_interval_length = sbjctend - sbjctstart
						the_coverage = float(the_template_interval_length)/sbjctlength
						SELC='NA-NA|'+the_template_interval+'|'+str(sbjctlength)+'|'+str(round(the_coverage,2))
						qinitial = querystart + intervalstart 
						qend = queryend +intervalstart
						sequence = queryline
						n=j
						# newdomainlist=domainlist
						del newdomainlist[j]
						newdomainlist.insert(n,'XXXX')
						newdomainlist.insert(n+1,the_cd_name)
						newdomainlist.insert(n+2,'XXXX')
						
						# newintervallist=intervallist
						oldstart=newintervallist[j].split("-")[0]
						firstnewinterval=oldstart+'-'+str(qinitial)
						the_interval=str(qinitial)+'-'+str(qend)
						oldend=newintervallist[j].split("-")[1]
						thirdnewinterval=str(qend)+'-'+oldend
						
						del newintervallist[j]
						newintervallist.insert(n,firstnewinterval)
						newintervallist.insert(n+1,the_interval)
						newintervallist.insert(n+2,thirdnewinterval)
					
						# newscorelist=scorelist
						del newscorelist[j]
						newscorelist.insert(n,'XXXX')
						newscorelist.insert(n+1,the_score)
						newscorelist.insert(n+2,'XXXX')
						
						# newdblist=dblist
						del newdblist[j]
						newdblist.insert(n,'XXXX')
						newdblist.insert(n+1,the_db_name)
						newdblist.insert(n+2,'XXXX')
						
						# newtemplateSELClist=templateSELClist
						del newtemplateSELClist[j]
						newtemplateSELClist.insert(n,'XXXX')
						newtemplateSELClist.insert(n+1,SELC)
						newtemplateSELClist.insert(n+2,'XXXX')
						
						# newsequencelist=sequencelist
						del newsequencelist[j]
						newsequencelist.insert(n,'XXXX')
						newsequencelist.insert(n+1,sequence)
						newsequencelist.insert(n+2,'XXXX')
						
						# newalignmentlist=alignmentlist
						del newalignmentlist[j]
						newalignmentlist.insert(n,'XXXX')
						newalignmentlist.insert(n+1,alignmentline)
						newalignmentlist.insert(n+2,'XXXX')
						
						for i in range(0,len(newdomainlist)):
							cprint (newdomainlist[i]+'\t'+newintervallist[i]+'\t'+str(newscorelist[i])+'\t'+newdblist[i]+'\t'+newtemplateSELClist[i]+'\t'+newsequencelist[i]+'\t'+newalignmentlist[i])

			
						#update the lists,break and startover. 
						domainlist=list(newdomainlist)
						intervallist=list(newintervallist)
						scorelist=list(newscorelist)
						dblist=list(newdblist)
						templateSELClist=list(newtemplateSELClist)
						sequencelist=list(newsequencelist)
						alignmentlist=list(newalignmentlist)
						
						if searchonce:
							searchisgoingon="No"
						break
					
							
					
					
	#Write the results to the table
	outputtable=table
	outf=open(outputtable,'w')	
	outf.write(header + '\n')
	outf.write(fullsequence + '\n')
	outf.write(TMinfo + '\n')
	outf.write(segInfo + '\n')
	outf.write(coilsInfo + '\n')
	
	cprint("Found domain number")
	cprint(Founddomainnum)
	cprint("_______________________")
	cprint("End of rpsblast")

	
	for i in range(0,len(domainlist)):
		text=domainlist[i]+'\t'+intervallist[i]+'\t'+scorelist[i]+'\t'+dblist[i]+'\t'+templateSELClist[i]+'\t'+sequencelist[i].strip()+'\t'+alignmentlist[i].replace("\n","")+'\n'
		cprint (text)
		outf.write(text)
	outf.close()

#myout = open("deneme.out","w")

def table2lists(tablefile):
	inputfile=open(tablefile,'r')
	
	domainlist=[]
	intervallist=[]
	scorelist=[]
	dblist=[]
	lines=inputfile.readlines()
	SELClist=[]
	sequencelist=[]
	alignmentlist=[]
	
	
	header=lines[0].replace("\n","")
	fullsequence=lines[1].replace("\n","")
	TMinfo=lines[2].replace("\n","")
	segInfo=lines[3].replace("\n","")
	coilsInfo=lines[4].replace("\n","")
	
#	myout.write(str(lines))

	newlines = []
	for line in lines:
		if line.startswith("XXXX"):
			newlines.append(line)

	del lines[0]	#remove header line
	del lines[0]	#remove sequence line
	del lines[0]	#remove TM line
	del lines[0]	#remove seg line
	del lines[0]	#remove coils line
	
	
	for line in lines:
#		myout.write("LINE is " + line + "\n")		
		line=line.replace("\n","")
		domainlist.append(line.split("\t")[0].replace(" ",""))
		intervallist.append(line.split("\t")[1].replace(" ",""))
		scorelist.append(line.split("\t")[2].replace(" ",""))
		dblist.append(line.split("\t")[3].replace(" ",""))
		SELClist.append(line.split("\t")[4].replace(" ",""))
		sequencelist.append(line.split("\t")[5])
		alignmentlist.append(line.split("\t")[6])
	
	
	
	return header,fullsequence,TMinfo,segInfo,coilsInfo,domainlist,intervallist,scorelist,dblist,SELClist,sequencelist,alignmentlist

def table2splitteddomaintable(tablename,percentage):
	header,fullsequence,TMinfo,segInfo,coilsInfo,domainlist,intervallist,scorelist,dblist,templateSELClist,sequencelist,alignmentlist=table2lists(tablename) #Convert table format to lists
	returnvalue=0
	gapfound=1
	while gapfound:
		gapfound=0
		for i in range(0,len(domainlist)):
			cprint( 'i:'+str(i))
			cprint( domainlist[i])
			if 'XXXX' not in domainlist[i]:
				#print domainlist[i]
				sequence=sequencelist[i]
				alignment=alignmentlist[i]
				
				##Get rid of the extra '-' character in the sequence and alignment
				newsequence=''
				newalignment=''
				for l in range(0,len(sequence)):
					if sequence[l] is not '-':
						newsequence=newsequence+sequence[l]
						newalignment=newalignment+alignment[l]						
				sequence=newsequence
				alignment=newalignment
				##Done
				
				seqarray=list(sequence)
				aliarray=list(alignment)
				
				# for j in range(0,len(seqarray)):
					# seqletter=seqarray[j]
					# aliletter=aliarray[j]
					# if letter is '-':
						# newsequence=newsequence+seqletter
						# newalignment=newalignment+aliletter
						
				theminimumgap=int(len(sequence)*float(percentage)/100)
				gappattern=''
				for k in range(0,theminimumgap):
					gappattern=gappattern+'_'
				cprint( gappattern)
				cprint( len(gappattern))
				theminimumaa=5
				cprint( alignment)
				cprint( theminimumgap)
				if (gappattern in alignment) and (len(gappattern)>theminimumaa):
					gapfound=1
					returnvalue=1
					#print gappattern
					#print domainlist[i]
					gappatternindex=alignment.index(gappattern) #Where the desired gap starts in the alignment without extra letters.
					p=gappatternindex
					theendpoint=0
					while alignment[p] is '_':
						theendpoint=p
						p=p+1
						#print alignment
						#print i
						#print p
						

					#print domainlist[i]
					newdomainlist=list(domainlist)
					del newdomainlist[i]
					# print domainlist[i]
					newdomainlist.insert(i,domainlist[i])
					newdomainlist.insert(i+1,'XXXX')
					newdomainlist.insert(i+2,domainlist[i])
					# print '-------------------'
					# print domainlist[i]
					# print newdomainlist[i]
					# print '-------------------'
					newintervallist=list(intervallist)
					oldstart=int(newintervallist[i].split("-")[0])
					thesplitbeg=gappatternindex-1+oldstart
					thesplitend=theendpoint+oldstart
					firstnewinterval=str(oldstart)+'-'+str(thesplitbeg)
					the_interval=str(thesplitbeg)+'-'+str(thesplitend)
					oldend=newintervallist[i].split("-")[1]
					thirdnewinterval=str(thesplitend)+'-'+oldend
					del newintervallist[i]
					newintervallist.insert(i,firstnewinterval)
					newintervallist.insert(i+1,the_interval)
					newintervallist.insert(i+2,thirdnewinterval)
			
					newscorelist=list(scorelist)
					del newscorelist[i]
					newscorelist.insert(i,scorelist[i])
					newscorelist.insert(i+1,'XXXX')
					newscorelist.insert(i+2,scorelist[i])
			
					newdblist=list(dblist)
					del newdblist[i]
					newdblist.insert(i,dblist[i])
					newdblist.insert(i+1,'XXXX')
					newdblist.insert(i+2,dblist[i])
					
					# SELC=the_template_interval+'|'+str(the_hmm_length)+'|'+str(round(the_coverage,2))
					oldSELC=templateSELClist[i]
					#print i
					#print oldSELC
					oldSELCzero=oldSELC.split('|')[0]
					oldSELCzero0=oldSELCzero.split('-')[0]
					oldSELCzero1=oldSELCzero.split('-')[1]
					newSELCzero0=max(oldSELCzero0,oldstart)
					newSELCzero1=min(oldSELCzero1,oldend)
					newSELCzero=str(newSELCzero0)+'-'+str(newSELCzero1)
					
					oldSELCinterval=oldSELC.split('|')[1]
					oldSintbeg=int(oldSELCinterval.split('-')[0])
					oldE=int(oldSELCinterval.split('-')[1])
					beg2=theendpoint-gappatternindex
					newSintend=int(oldSintbeg)+int(gappatternindex)
					newS2=newSintend+1
					newratio1=(newSintend-oldSintbeg)/float(oldSELC.split('|')[2])
					newratio2=(oldE-beg2)/float(oldSELC.split('|')[2])
					
					SELC1='NA-NA|'+str(oldSintbeg)+'-'+str(newSintend)+'|'+str(oldSELC.split('|')[2])+'|'+str(newratio1)
					SELC2='NA-NA|'+str(newS2)+'-'+str(oldE)+'|'+str(oldSELC.split('|')[2])+'|'+str(newratio2)
					
					newtemplateSELClist=list(templateSELClist)
					del newtemplateSELClist[i]
					newtemplateSELClist.insert(i,SELC1)
					newtemplateSELClist.insert(i+1,'XXXX')
					newtemplateSELClist.insert(i+2,SELC2)
			
					newsequencelist=list(sequencelist)
					del newsequencelist[i]
					newsequencelist.insert(i,sequence[:gappatternindex])
					newsequencelist.insert(i+1,'XXXX')
					newsequencelist.insert(i+2,sequence[theendpoint:])
			
					newalignmentlist=list(alignmentlist)
					del newalignmentlist[i]
					newalignmentlist.insert(i,alignment[:gappatternindex])
					newalignmentlist.insert(i+1,'XXXX')
					newalignmentlist.insert(i+2,alignment[theendpoint:])
					
					
					#for i in range(0,len(newdomainlist)):
					cprint (newdomainlist[i]+'\t'+newintervallist[i]+'\t'+str(newscorelist[i])+'\t'+newdblist[i]+'\t'+templateSELClist[i]+'\t'+newsequencelist[i]+'\t'+newalignmentlist[i])
			
					#update the lists,break and startover. 
					domainlist=list(newdomainlist)
					intervallist=list(newintervallist)
					scorelist=list(newscorelist)
					dblist=list(newdblist)
					templateSELClist=list(newtemplateSELClist)
					sequencelist=list(newsequencelist)
					alignmentlist=list(newalignmentlist)
					
					break
				
			i=i+1
			
	outf=open(tablename,'w')
        outf.write(header + '\n')
        outf.write(fullsequence + '\n')
        outf.write(TMinfo + '\n')
        outf.write(segInfo + '\n')
        outf.write(coilsInfo + '\n')
	
	for i in range(0,len(domainlist)):
		text=domainlist[i]+'\t'+intervallist[i]+'\t'+scorelist[i]+'\t'+dblist[i]+'\t'+templateSELClist[i]+'\t'+sequencelist[i]+'\t'+alignmentlist[i]+'\n'
		#cprint (text)
		outf.write(text)
	outf.close()
	return returnvalue
	
		
def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return		
		

def get_sgeID():
	#statusfiles = glob("STATUS*")
        crossIDfile = open("crossID","r")
        return int(crossIDfile.readline().strip().split(".")[0])
        #crossIDfile.readline()
        #thirdline = crossIDfile.readline()
        #if thirdline:
        #        sgeID = int(thirdline[0:7])
        #else:
         #       sgeID = 0

	#return sgeID
	

		
# removefile('cprint.txt')
# removefile('detailed_output.txt')

# detailedoutput=open('detailed_output.txt','w')
# datestring=str(timestart.month)+'/'+str(timestart.day)+'/'+str(timestart.year)+'-'+str(timestart.hour)+':'+str(timestart.minute)
# detailedoutput.write('Job started on '+datestring)
# detailedoutput.write("\n_________________________\n")
# detailedoutput.close()

if newton:
	code = "qstat >STATUS" + ID
	os.system(code)

for proteinheader in LIST:
	uniquename=codost.removesymbols(proteinheader)[0:50]
	tablename=uniquename+'.table'
	if cont==1 and os.path.isfile(tablename):
		print "This table is already present, We are skipping this protein"
	else:
		
		if newton:
			tablename=ID+tablename
		fullsequence=DIC[proteinheader]
		
		fa2nulltable(proteinheader,fullsequence)
		
		if runTM:
			addTMtotable(tablename,TMmethod)
		
		
		if runhmmer:
			table2pfamscantable(tablename)
		
		if rpsblast:
			table2RPStable(tablename,rpsDB,rpsEvalue,rpsGap)
			
	
		if (dbinputtext is not "none") and (dbinputtext is not "None"):
			dbs=dbinputtext.split("AND")
			for db in dbs:
				datab=db.split("_")[0]
				if len(db.split("_"))==3:
					proba=max(20.0,float(db.split("_")[1]))
					gapl=max(10,int(math.floor(float(db.split("_")[2]))))
					
				else:
					proba=80.0
					gapl=30
					
				table2hhpredtable(tablename,datab,proba,gapl) #MASTER!
				
				if splitdomain:
					splitted=table2splitteddomaintable(tablename,splitpercentage) 
					#It will split the domain if there is a good gap in the alignment and return the value 1 if there is gap, it gives "Zero" if nothing found 
					if splitted:
						table2hhpredtable(tablename,datab,proba,gapl) #Because new GAP is formed after domain split, we need to search that gap one more time!
		
		
if not '--nodraw' in sys.argv:
	drawcode="python "+pathto['table2domfigure']+" -i "+inputfile+" -o "+outputfile
	try:
		os.system(drawcode)
		cprint (outputfile+' is created successfully!')
	except:
		pass
	
if not newton:
	try:
		removefile(" *a3m")
	except:
		pass
	

def sendTheTable(tablename,theOrder):

	sgeID = get_sgeID()
	code = pathto["sendAfile"] + ' '  + tablename + " " + str(sgeID) + " " + theOrder
	max_trials = 5
	count = 0
	success = 1
	while success!=0 and count<max_trials:
		success = os.system(code)
		count += 1

def executeTable2DomFigureOnLeo(htmlID):
	code = 'ssh -p 32790 root@leonidas.utk.edu -t -t " cd /var/www/cdvist.utk.edu/htdocs/codost/scripts; python table2domfigure_nv1.py --empty -i ../FASTA/FASTA' + htmlID + '.fa -o ../codost_out/' + htmlID + '.html -newton ' + htmlID + '" >table2dom.log' 	
	os.system(code)

if newton:
	print "At the line of MYS4565"
	jsonname = tablename[0:-5] + "json"
	jsontext = codost.table2jsonObject(tablename)
	with open(jsonname,"w") as outfile:
		outfile.write(jsontext)
	sendTheTable(jsonname,theOrder)
	code = "qstat >STATUS" + ID
	os.system(code)
	#executeTable2DomFigureOnLeo(ID)
else:
	pass







	
timeend=datetime.datetime.now()


# removefile('tempTM.fa')
# removefile('tempTMout.txt')



timepassed=timeend-timestart

cprint("Time spent:")
cprint(str(timepassed))
if printsign==0:
	cprintfile.close()
