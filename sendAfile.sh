#!/bin/bash

file=$1

ID=${file:0:20}
echo $ID
cd /home/oadebali/codost/requests/$ID
sed -n '3,$p' CODOST_$ID.o$2.$3 >CDvist_$ID.$3.log
sed -n '1,$p' CODOST_$ID.e$2.$3 >CDvist_$ID.$3.error
sftp -oPort=32790 root@leonidas.utk.edu:/var/www/cdvist.utk.edu/htdocs/codost/codost_out <<EOF
put $1
put CDvist_$ID.$3.log
put CDvist_$ID.$3.error
EOF

#rm $1*.table


#/home/oadebali/codost/scripts/qsubcodost.py



#FASTAFILES=FASTA*.fa
#for file in $FASTAFILES
#do
#subfile=${file:5:20}
#echo '-----'
#echo $subfile
#done

#qsub /home/oadebali/codost/requests/$subfile.sge >$subfile.ID
