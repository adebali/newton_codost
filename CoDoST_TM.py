#!/usr/bin/env python
#####by###OGUN ADEBALI###########
####UNIVERSITY OF TENNESSEE######
#Last Update: 28 November 2013
#################################
import os
import glob
import sys
import datetime
import re
import codostlib as codost
timestart=datetime.datetime.now()
arg=[]
arg = sys.argv
help = "\n__________\nThis script takes fasta file as an input, runs pfamscan(HMMER3) and/or hhpred, produces a table for each sequence in your fasta"
help = help+"\n\nusage: CoDoST.py -i yourfastafile.fa -d database_Probabilitycutoff_gapcutoffANDseconddatabase_secondprobability_secondgap\noptional: \n-o output.html"
help = help+"\n\nPS: In your fasta file first 50 characters of each header must be unique!\n"
help = help+"\n\nExample: CoDoST.py -i myfasta.fa -d pfam_90_50ANDcdd_85_30ANDpdb_95_25\n"
help = help+"\n\nOR: CoDoST.py -i myfasta.fa -d pfam_ANDcddANDpdb (default values are 90_50 when not defined)\n"

pathto={}
#Leonidas
# pathto['pfam_scan']='/home/ogun/scripts/PfamScan/pfam_scan.pl'
# pathto['pfam_scandb']='/home/ogun/allDB/Pfam27.0'
# pathto['hhsearchdb']='/home/ogun/allDB/hhpredDB/hhsearch_dbs/'
# pathto['hhsearch']='/home/ogun/tools_o/hhpred/hhsuite-2.0.16/bin/hhsearch'
# pathto['table2domfigure']='/home/ogun/scripts/table2domfigure14.py'
#god
pathto['pfam_scan']='/home/oadebali/codost/scripts/PfamScan/pfam_scan.pl'
pathto['pfam_scandb']='/home/oadebali/codost/db'
pathto['hhsearchdb']='/home/oadebali/codost/db/'
pathto['hhsearch']='/home/oadebali/codost/hhpred/hhsuite-2.0.16/bin/hhsearch'
pathto['table2domfigure']='/home/oadebali/codost/scripts/table2domfigure151.py'
pathto['phobius']='/home/oadebali/codost/scripts/phobius/phobius.pl'
pathto['tmhmm']='/home/oadebali/codost/scripts/TMHMM2/tmhmm-2.0c/bin/tmhmm'



if '-h' in sys.argv:
		print "HELP INFO!"+help+"\n"
		sys.exit()


if '-tm' in sys.argv:
	TMmethod=sys.argv[sys.argv.index('-tm')+1]
else:
	TMmethod="tmhmm"
	

	
printsign=1
def cprint(thetexttobeprinted):
	if printsign:
		print thetexttobeprinted
		return
	else:
		cprintfile.write(str(thetexttobeprinted))
		cprintfile.write('\n')
		return
		

def addTMtotable(table,TMmethod):
	lines=open(table,'r').readlines()
	TM_name=table[:-5]+"fa"
	TM_out=table[:-6]+"_out.txt"
	temp=open(TM_name,'w')
	w='>'+lines[0]+'\n'+lines[1]
	temp.write(w)
	
	temp.close()
	if "phobius" in TMmethod:
		code="perl "+pathto['phobius']+" -short "+TM_name+" >"+TM_out
		os.system(code)
		outline=open(TM_out,'r').readlines()[1]
		outline=' '.join(outline.split())
		templist=outline.split(' ')
		del templist[0]
		newoutline='_'.join(templist)+'\n'
		del lines[2]
		cprint(newoutline)
		lines.insert(2,newoutline)
		cprint(lines[2])
		outfile=open(table,'w')
		for line in lines:
			outfile.write(line)
		outfile.close()
		removefile(TM_name)
		removefile(TM_out)
	elif 'tmhmm' in TMmethod:
		code=pathto['tmhmm']+" -short "+TM_name+" >"+TM_out
		os.system(code)
		outline=open(TM_out,'r').readlines()[0]
		predhel=outline.split('\t')[4].split('=')[1]
		topology=outline.split('\t')[5].split('=')[1]
		newoutline=predhel+'_'+'0'+'_'+topology+'\n'
		del lines[2]
		cprint('_________________')
		cprint(newoutline)
		lines.insert(2,newoutline)
		cprint(lines[2])
		outfile=open(table,'w')
		for line in lines:
			outfile.write(line)
		outfile.close()
		removefile(TM_name)
		removefile(TM_out)
	else:
		cprint("no correct TM prediction method is stated")
		return 0
	
	
def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return
	



for file in glob.glob("*.table"):
	addTMtotable(file,TMmethod)
	