#!/usr/bin/env python
#####by###OGUN ADEBALI###########
####UNIVERSITY OF TENNESSEE######
#Last Update: 16 January 2013
#################################

import os
import sys
import time
import glob
import subprocess

def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	
	data.close()
	return seq_dic, list_order




for newfile in glob.glob("/home/oadebali/codost/requests/NEW*"):
	
	ID=newfile.split('NEW')[1]
	particularpath = "/home/oadebali/codost/requests/" + ID
	os.chdir(particularpath)
	fastafile = "/home/oadebali/codost/requests/"+ ID + "/" + "FASTA" + ID + ".fa"
	paramfile = "/home/oadebali/codost/requests/"+ ID + "/" + "PARAM" + ID + ".txt"

	DIC,LIST=fastareader(fastafile)
	totalfiles=len(LIST)
	# ID=fastafile.split('FASTA')[1][-3]

	#print fastafile
	print ID

	parameters=open(paramfile,'r').readlines()[0]
	joblength = "short"
	joblength = "medium"
	if '-joblength' in parameters:
		joblength = parameters.split('-joblength')[1].strip().split(' ')[0].strip()
	# DIC,LIST=fastareader(newfastafile)
	for i in range(0,len(LIST)):
		j=i+1
		header=LIST[i]
		individualfilename='/home/oadebali/codost/requests/'+ ID + "/" + ID + '_' + str(j) + '.fa'
		fastatext='>'+header+'\n'+DIC[header]
		indf=open(individualfilename,'w')
		indf.write(fastatext)
		indf.close()
	jobfilename='/home/oadebali/codost/requests/' + ID + "/" + ID + '.sge'
	fastafullpath = '/home/oadebali/codost/requests/' + ID + "/" + ID 
	jobf=open(jobfilename,'w')
#$ -l mem=24G
	#joblength = ''
	text='''#!/bin/bash
#$ -N CODOST_'''+ID+'''
#$ -cwd
#$ -q '''+joblength+'''*
#$ -t 1-'''+str(totalfiles)+'''
setenv PATH ${PATH}:/home/oadebali/python/Python-2.7.2:/home/oadebali/perl5/bin:/home/oadebali/python/Python-2.7.2:/usr/lib64/qt-3.3/bin:/data/apps/emacs/23.2/bin:/data/apps/R/3.0.1/bin:/data/apps/site_utilities:/data/apps/openmpi/1.4.3-intel-psm/bin:/data/apps/MATLAB/R2013a/bin:/data/apps/intel/2011.5.220/composerxe-2011.5.220/bin/intel64:/data/apps/intel/2011.5.220/composerxe-2011.5.220/mpirt/bin/intel64:/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/home/oadebali/bin:/home/oadebali/codost/scripts:/home/oadebali/codost/scripts/PfamScan:/home/oadebali/scripts:/home/oadebali/bin:/lustre/projects/ogun_data/tools/bin:/home/oadebali/codost/scripts/PfamScan
setenv PERL5LIB /home/oadebali/perl5/lib/perl5:/lustre/projects/ogun_data/codost/scripts/PfamScan
setenv COILSDIR /lustre/projects/ogun_data/codost/scripts/coils2
#CoDoST1.51.py -i '''+fastafullpath+'''_$SGE_TASK_ID.fa '''+parameters+'''
CDvist.py -i '''+fastafullpath+'''_$SGE_TASK_ID.fa '''+parameters+'''
echo 'done'
'''
	jobf.write(text)
	jobf.close()
	

	QSUB = "qsub " + jobfilename
	jobsubmissionline = subprocess.check_output(QSUB, shell=True)
	print("________")
	print(jobsubmissionline)
	print("________")
	sgeID = jobsubmissionline.split("array")[1].split("-")[0]
	
	os.system("echo " + sgeID + "  >crossID")
	os.system("qstat >STATUS" + ID)	

	oldfile=newfile.replace("NEW","OLD")
	code='mv '+newfile+' '+ oldfile
	os.system(code)


