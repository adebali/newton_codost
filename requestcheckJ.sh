#!/bin/sh

################INITIALIZATION################
#echo ___________
#echo $SHELL
SHELL=/bin/bash
#echo $SHELL
#echo _____________
#echo $PATH
echo $(date)
#VARI=/opt/sge/bin/lx24-amd64
PATH=$PATH:/opt/grid/bin/lx26-amd64:/home/oadebali/python/Python-2.7.2:/home/oadebali/perl5/bin:/home/oadebali/python/Python-2.7.2:/opt/grid/bin/lx26-amd64:/usr/lib64/qt-3.3/bin:/data/apps/emacs/23.2/bin:/data/apps/R/3.0.1/bin:/data/apps/site_utilities:/data/apps/openmpi/1.4.3-intel-psm/bin:/data/apps/MATLAB/R2013a/bin:/data/apps/intel/2011.5.220/composerxe-2011.5.220/bin/intel64:/data/apps/intel/2011.5.220/composerxe-2011.5.220/mpirt/bin/intel64:/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/home/oadebali/bin:/home/oadebali/codost/scripts:/home/oadebali/codost/scripts/PfamScan:/home/oadebali/scripts:/home/oadebali/bin:/lustre/projects/ogun_data/tools/bin:/home/oadebali/codost/scripts/PfamScan
export PATH
export SGE_ROOT=/opt/grid
PERL5LIB=/home/oadebali/perl5/lib/perl5:/lustre/home/oadebali/codost/scripts/PfamScan:
export PERL5LIB
###############################################


################# GET THE FILES ###############
cd ~/codost/requests
sftp -oPort=32790 root@leonidas.utk.edu:/var/www/cdvist.utk.edu/htdocs/codost/requests <<EOF
get FASTA*.fa
get PARAM*.txt
rm FASTA*
rm PARAM*
EOF



###############################################



#echo $PATH


########## pybash #########################
python /home/oadebali/codost/scripts/pybashJ.py
###########################################


########## qsubcodost #####################
/home/oadebali/codost/scripts/qsubcodostJ.py
###########################################



#qsub /home/oadebali/codost/requests/$subfile.sge >$subfile.ID
#sleep 1
#/home/oadebali/codost/scripts/resultscheck.py
