import fileinput
from math import *
import sys
import os
#import numpy
import md5
import base64
from Bio import Entrez, SeqIO
import toolib



def interval_diff(int1,int2):
#This function takes two intervals on protein (could be domain regions or TM regions etc)
#And gives a dictionary that tells us if they overlap, if so how.
#Second interval is considered as query, first one is template
#eg: TM regions: First interval is known TM region, second interval is prediction.
	
	int1=int1.strip()
	int2=int2.strip()
	position='NA'
	overlap=0
	leftextension=0
	rightextension=0

	
	if ('-' not in int1) or (len(int1.split('-')) != 2):
		if ('-' not in int2) or (len(int2.split('-')) != 2) or (int2 == 0):
			overlap=0
			truepositive=0
			falsepositive=0
			falsenegative=0
			interval2length=0
			dict={
				'position':position,
				'leftextension':leftextension,
				'overlap':overlap,
				'rightextension':rightextension,
				'falsepositive':falsepositive,
				'falsenegative':falsenegative,
				'interval2length':interval2length
				}
			return dict
		else:
			overlap=0
			truepositive=0
			falsenegative=0
			s2=int(int2.split('-')[0])
			e2=int(int2.split('-')[1])
			interval2length=e2-s2
			falsepositive=interval2length
			
			dict={
				'position':position,
				'leftextension':leftextension,
				'overlap':overlap,
				'rightextension':rightextension,
				'falsepositive':falsepositive,
				'falsenegative':falsenegative,
				'interval2length':interval2length
				}
			return dict

	if ('-' not in int2) or (len(int2.split('-')) != 2) or (int2 == 0):
		overlap=0
		truepositive=0
		falsepositive=0
		falsenegative=int(int1.split('-')[1])-int(int1.split('-')[0])+1
		interval2length=0
		dict={
			'position':position,
			'leftextension':leftextension,
			'overlap':overlap,
			'rightextension':rightextension,
			'falsepositive':falsepositive,
			'falsenegative':falsenegative,
			'interval2length':interval2length
			}
		return dict
	
	s1=int(int1.split('-')[0])
	e1=int(int1.split('-')[1])
	s2=int(int2.split('-')[0])
	e2=int(int2.split('-')[1])
	
	interval2length=e2-s2
	
	if s1 == e1:
		pass
	elif s1>e1:
		t=s1
		s1=e1
		e1=t	
		
	if s2 == e2:
		pass
	elif s2>e2:
		t=s2
		s2=e2
		e2=t
		
	#mydic={'s1':s1,'e1':e1,'s2':s2,'e2':e2}
	

	mylist=[s1,e1,s2,e2]
	mysortedlist=sorted(mylist)

	s1i=mysortedlist.index(s1)
	s2i=mysortedlist.index(s2)
	e1i=mysortedlist.index(e1)
	e2i=mysortedlist.index(e2)
	
	indexlist=[s1i,e1i,s2i,e2i]
	
	#print indexlist
	

	NOR=[0,1,2,3]
	NOL=[2,3,0,1]
	REX=[0,2,1,3]
	LEX=[1,3,0,2]
	OIN=[0,3,1,2]
	OOU=[1,2,0,3]
	
	truepositive=0
	falsepositive=0
	falsenegative=0
	
	
	if indexlist == NOR : #No overlap-right [0,1,2,3]
		position="NOR"
		overlap=0
		leftextension=e1-s1+1
		rightextension=e2-s2+1
		falsepositive=rightextension
		falsenegative=leftextension
	elif indexlist == NOL: #No overlap-left [2,3,0,1]
		position="NOL"
		overlap=0
		leftextension=e2-s2+1
		rightextension=e1-s1+1
		falsepositive=leftextension
		falsenegative=rightextension
	elif (indexlist == REX) or (indexlist == [0,1,1,3]): #overlap-right_extension [0,2,1,3]
		position='REX'
		overlap=e1-s2+1
		leftextension=s2-s1
		rightextension=e2-e1
		falsepositive=rightextension
		falsenegative=leftextension
	elif (indexlist == LEX) or (indexlist == [1,3,0,1]): #overlap-left_extension [1,3,0,2]
		position='LEX'
		overlap=e2-s1+1
		leftextension=s1-s2
		rightextension=e1-e2
		falsepositive=leftextension
		falsenegative=rightextension
	elif (indexlist == OIN) or (indexlist == [0,3,0,2]) or (indexlist == [0,2,1,2]) or (indexlist == [0,2,0,2]) or (indexlist == [0,3,1,1]): #overlap_in [0,3,1,2]
		position='OIN'
		overlap=e2-s2+1
		leftextension=s2-s1
		rightextension=e1-e2
		falsepositive=0
		falsenegative=leftextension+rightextension
	elif (indexlist == OOU) or (indexlist == [0,2,0,3]) or (indexlist == [1,2,0,2]) or (indexlist == [1,1,0,3]): #overlap_out [1,2,0,3]
		position='OOU'
		overlap=e1-s1+1
		leftextension=s1-s2
		rightextension=e2-e1
		falsenegative=0
		falsepositive=leftextension+rightextension
	else:
		print s1,e1,s2,e2
		print indexlist
		print 'Something is wrong in interval_diff function. Exiting...'
		sys.exit()
		
	truepositive=overlap
	
	dict=	{
			'position':position,
			'leftextension':leftextension,
			'overlap':overlap,
			'rightextension':rightextension,
			'falsepositive':falsepositive,
			'falsenegative':falsenegative,
			'interval2length':interval2length
			}
	
	
	
	
	return dict
	
	
		
	
	
		
def file2unix(file):
	infile=open(file,'r')
	lines=[]
	lines=infile.readlines()
	infile.close()
	newline=''
	for line in lines:
		newline+=line
	outfile=open(file,'w')
	outfile.write(newline.replace('\r\n','\n').replace('\r','\n'))
	outfile.close()

def removesymbols(text):
	return text.replace(' ','_').replace('[','_').replace(']','_').replace('{','_').replace('}','_').replace(':','_').replace('\n','').replace(';','_').replace('|','_').replace(',','_').replace('(','').replace(')','')

def nwkupdate(newick,fa):
	nwk=open(newick,'r')
	newnwkname=newick.replace('.nwk','_updated.nwk')
	newnwk=open(newnwkname,'w')
	nwklines=[]
	nwklines=nwk.readlines()
	s, idl=toolib.fastareader(fa)
	mydic=dict()
	for header in idl:
		code=header[4:13]
		mydic[code]=toolib.removesymbols(header)
	for line in nwklines:
		for keys in mydic:
			line=line.replace(keys,mydic[keys])
		newnwk.write(line)
	nwk.close()
	newnwk.close()


def gi2tm(gi):
	return toolib.faseq2tm(toolib.gi2faseq(gi))

def faseq2tmtrim(faseq,intracellular,predictedTM):
	#print faseq
	fatemp=open('fatemp.txt','w')
	fatemp.write(faseq)
	fatemp.close()
	cmd="tmhmm -short fatemp.txt >tmtemp.txt"
	
	#print cmd
	os.system(cmd)
	tmfile=open("tmtemp.txt",'r')
	lines=tmfile.readlines()
	topology=lines[0].split('\t')[5].replace('\n','')
	if '=' in topology:
		topology=topology.split('=')[-1]
		
	PredHel=int(lines[0].split('\t')[4].replace('\n','').split('=')[1])
	if PredHel == predictedTM:
		#print PredHel
		#print topology
		range=topology.split('-')
		#print range[intracellular-1].split('-')[1]
		intras=[]
		for part in range:
			if 'i' in part:
				intras.append(part)
				print part
		intracellular=intracellular-1
		if intras[intracellular].split('i')[0]:
			st=int(intras[intracellular].split('i')[0])
		else:
			st=0
		if intras[intracellular].split('i')[1]:
			en=int(intras[intracellular].split('i')[1])
		else:
			en=len(faseq.split('\n')[1])

		
		return faseq.split('\n')[1][st:en]
		os.remove("tmtemp.txt")
		os.remove("fatemp.txt")
	else:
		print 'predicted TM number is not consistent with your input\n exiting...'
		print faseq.split('\n')[0]
		print topology
		sys.exit()
	
def faseq2tm(faseq):
	#print faseq
	fatemp=open('fatemp.txt','w')
	fatemp.write(faseq)
	fatemp.close()
	cmd="tmhmm -short fatemp.txt >tmtemp.txt"
	
	#print cmd
	os.system(cmd)
	tmfile=open("tmtemp.txt",'r')
	lines=tmfile.readlines()
	PredHel='NA'
	topology='NA'
	if (lines[0].split('\t')[4]):
		topology=lines[0].split('\t')[5].replace('\n','')
		PredHel=lines[0].split('\t')[4].replace('\n','').split('=')[1]
	return PredHel, topology
	os.remove("tmtemp.txt")
	os.remove("fatemp.txt")
	os.system("rm -r TMHMM*")

def gi2faseq(gi):
	return '>'+gi2des(gi)+'\n'+gi2seq(gi)

def gi2png(gi):
	return toolib.md52png(toolib.seq2md5(toolib.gi2seq(gi)))

def seq2md5(seq):
	return base64.encodestring(md5.new(seq.replace('-','')).digest()).replace('/','_').replace('=','').replace('+','-').replace('\n','')

def md52png(md5):
	return 'http://seqdepot.net/api/v1/aseqs/' +md5+ '.png'
	
def gi2seq(gi):
#Input should be only numbers
	Entrez.email = 'ogunadebali@gmail.com'
	handle = Entrez.efetch(db="protein", rettype="fasta", retmode="text", id=gi)
	seq=SeqIO.read(handle, "fasta")
	#fullinfo='>'+seq.description+'\n'+seq.seq+'\n'
	return str(seq.seq)
	
def gi2des(gi):
# gi number to Description line
#Input should be only numbers
	Entrez.email = 'ogunadebali@gmail.com'
	handle = Entrez.efetch(db="protein", rettype="fasta", retmode="text", id=gi)
	seq=SeqIO.read(handle, "fasta")
	#fullinfo='>'+seq.description+'\n'+seq.seq+'\n'
	return seq.description


def gi2des_seq(gi):
#Input should be only numbers
	Entrez.email = 'ogunadebali@gmail.com'
	handle = Entrez.efetch(db="protein", rettype="fasta", retmode="text", id=gi)
	seq=SeqIO.read(handle, "fasta")
	#fullinfo='>'+seq.description+'\n'+seq.seq+'\n'
	return seq.description, str(seq.seq)


def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order
	
def mist2tax(mist_id):
	link='http://www.mistdb.com/bacterial_genomes/summary/'+mist_id
	code="wget -O index.html "+link
	os.system(code)
	indexfile=open("index.html",'r')
	htmllist=indexfile.readlines()
	taxonomylist=""

	if not taxonomylist:
		for line in htmllist:
			if "<th>Taxonomy" in line:
				tax_class_line=htmllist[htmllist.index(line)+1]
				taxonomylist=tax_class_line.split(">")[1].split("<")[0]			
	os.system("rm index.html")			
	return taxonomylist
		
		
