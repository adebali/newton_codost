#!/usr/bin/env python
#####by###OGUN ADEBALI###########
####UNIVERSITY OF TENNESSEE######
#Last Update: 18 May 2014
#################################
import os
import sys
import datetime
import re
import json
from glob import glob

def finalMergeJsonObjects(ID):
        mainJSONfile = ID + ".json"
        with open(mainJSONfile) as data_file:
                mainJ = json.load(data_file)

        #Jfiles = glob("_*.json")

	for e in mainJ["entries"]:
		num = e["num"]
		Jfile = "_" + str(num) + ".json"	
		if os.path.isfile(Jfile):
			with open(Jfile) as data_file:
				jData = json.load(data_file)
			jData["status"] = "completed"
			jData["num"] = num
			jData["header"] = mainJ["entries"][num-1]["header"]
			mainJ["entries"][num-1] = jData
		else:
			mainJ["entries"][num-1]["status"] = "killed"


	with open(ID + ".json", "w") as outfile:
		json.dump(mainJ, outfile, indent=4)



def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order



def removesymbols(text):
	if re.match('^[0-9]',text):
		text = "N" + text
	return text.replace(' ','_').replace('[','_').replace(']','_').replace('{','_').replace('}','_').replace(':','_').replace('\n','').replace(';','_').replace('|','_').replace(',','_').replace('(','_').replace(')','_').replace('/','_').replace('\\','_').replace('\'','_').replace(".","")


def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order
	
def deneme():
	return "poopo"
		

def table2jsonObject(table_in):
	table=open(table_in,'r')
	lines=table.readlines()
	
	header=lines[0].replace('\n','')
	fullsequence=lines[1].replace('\n','')
	TMinfo=lines[2].replace('\n','')
	segInfo=lines[3].replace('\n','')
	coilsInfo=lines[4].replace('\n','')
	del lines[0]
	del lines[0]
	del lines[0]
	del lines[0]
	del lines[0]
	tag=removesymbols(header)[:50]
	
	seqlength=len(fullsequence)
	
	features='\''+tag+'\''+','+'\''+str(fullsequence)+'\''+','+'\''+str(TMinfo)+'\''+','
	domains=''
	intervals=''
	scores=''
	databases=''
	coverages=''
	sequences=''
	alignments=''
	
	
	for line in lines:
		if line:
			if not 'XXXX' in line.split('\t')[0]: 
				
				domains=domains+line.split('\t')[0]+';'
				intervals=intervals+line.split('\t')[1]+';'
				scores=scores+line.split('\t')[2]+';'
				databases=databases+line.split('\t')[3]+';'
				sequences=sequences+line.split('\t')[5].replace("\n","")+';'
				alignments=alignments+line.split('\t')[6].replace("\n","").replace(" ","_")+';'
				
				
				cov=line.split('\t')[4].split('|')[0].strip()+'_'+line.split('\t')[4].split('|')[1].strip()+'_'+line.split('\t')[4].split('|')[2].strip()
				coverages=coverages+cov+';'
			
		
	features=features+'\''+domains+'\''+','+'\''+intervals+'\''+','+'\''+scores+'\''+','+'\''+databases+'\''+','+'\''+coverages+'\''+','+'\''+sequences+'\''+','+'\''+alignments+'\''
	
	dl = domains.split(';')
	il = intervals.split(';')
	sl = scores.split(';')
	dbl = databases.split(';')
	cl = coverages.split(';')
	sel = sequences.split(';')
	al = alignments.split(';')
	
	data = {"status":"completed","header" : header, "fullsequence" : fullsequence, "TM" : TMinfo, "seg":segInfo, "coils":coilsInfo, "segments" : []}
	myjson = json.loads(json.dumps(data))
	for i in range(0,len(dl)-1):
		if dl[i] != "XXXX":
			#segment = { count : { "domain": dl[i], "interval" : il[i], "score" : sl[i], "database" : dbl[i], "coverage" : cl[i], "sequence" : sel[i], "alignment" : al[i]} }
			segment = { "domain": dl[i], "interval" : il[i], "score" : sl[i], "database" : dbl[i], "coverage" : cl[i], "sequence" : sel[i], "alignment" : al[i]} 
			#myjson["segments"].update(segment)
			myjson["segments"].append(segment)
			#myjson["segments"] = myjson["segments"],segment

	j = json.dumps(myjson, sort_keys=True, indent= 4)
	

	return j
