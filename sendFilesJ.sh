#!/bin/bash

ID=$1
num=$2
echo $ID
echo $2

cd /home/oadebali/codost/requests/$ID


#JOBID=${$(ls CODOST_$ID.e* | head -n 1 | cut -d'.' -f2):1}
eJOBID=$(ls CODOST_$ID.e* | head -n 1 | cut -d'.' -f2)
oJOBID=$(ls CODOST_$ID.o* | head -n 1 | cut -d'.' -f2)

sed -n '3,$p' CODOST_$ID.$oJOBID.$2 >CDvist_$ID.$2.log
sed -n '1,$p' CODOST_$ID.$eJOBID.$2 >CDvist_$ID.$2.error
if [ $num ]; then
 sftp -oPort=32790 root@leonidas.utk.edu:/var/www/cdvist.utk.edu/htdocs/codost/codost_out/$1/ <<EOF
put $1.json
put _$2.json
put CDvist_$ID.$2.log
put CDvist_$ID.$2.error
EOF
else
 sftp -oPort=32790 root@leonidas.utk.edu:/var/www/cdvist.utk.edu/htdocs/codost/codost_out/$1/ <<EOF
put *.json
put CDvist_*log
EOF
fi

#rm $1*.table


#/home/oadebali/codost/scripts/qsubcodost.py



#FASTAFILES=FASTA*.fa
#for file in $FASTAFILES
#do
#subfile=${file:5:20}
#echo '-----'
#echo $subfile
#done

#qsub /home/oadebali/codost/requests/$subfile.sge >$subfile.ID
