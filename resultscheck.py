#!/usr/bin/env python
#####by###OGUN ADEBALI###########
####UNIVERSITY OF TENNESSEE######
#Last Update: 16 January 2013
#################################

import os
import sys
import time
import glob

def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order

def sendfile(ID):
	code="/home/oadebali/codost/scripts/sendfile.sh "+ID
	os.system(code)

def removefile(file):
        if os.path.isfile(file):
                code='rm '+file
                os.system(code)
                return
        else:
                return

print (time.strftime("%d/%m/%Y"))
print time.strftime("%H:%M:%S")
#if os.path.isfile('/home/oadebali/codost/requests/*ID'):
for idfile in glob.glob("/home/oadebali/codost/requests/*ID"):
	ID=idfile[-23:-3]
	print 'Cheking for '+ID+' ...'
	print idfile
	idf=open(idfile,'r')
	jobID=idf.readlines()[0].split(".")[0][15:]
	print jobID
	os.system("qstat >/home/oadebali/codost/requests/qstat.txt")
	statusf=open('/home/oadebali/codost/requests/qstat.txt','r')
	statuslines=statusf.readlines()
	totalstatus=''	
	for element in statuslines:
		totalstatus=totalstatus+element

	if (not statuslines) or (jobID not in totalstatus):
		print jobID
		print "CK2"
		os.chdir("/home/oadebali/codost/requests/")
		fastafile="CHECKPOINTTWO"+ID+".fa"
		param="PARAM"+ID+".txt"
		outputhtml=fastafile[13:-2]+"html"
		code="table2domfigure152.py -newton "+ID+" "+jobID+" -param "+param+" --print -i "+fastafile+" -o "+outputhtml
		print code
		os.system(code)
		sendfile(ID)
		
		removefile(outputhtml)
		code="rm CODOST_"+ID+"*"
		os.system(code)
		code="rm "+ID+"*fa"
		os.system(code)
		removefile(fastafile)
		removefile(param)
		sgefile=ID+".sge"
		removefile(sgefile)
		removefile(idfile)
		IDtables=ID+"*.table"
		removefile(IDtables)
#		a3mfiles=ID+"*a3m"
#		removefile(a3mfiles)
				
		

		#print 'if'
	else:
		print 'job '+ID+' is not completed yet!'


			
			
		
				

	
	
