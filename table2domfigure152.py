#!/usr/bin/env python


import os
import sys
import datetime
import re
import codostlib as co


timestart=datetime.datetime.now()

arg=[]
arg = sys.argv
help = "Help Info\n__________\nThis script takes fasta file as an input, reads headers, finds the tables, and uses the tables as inputs"
help = help+"\n\nusage: table2domfigure.py -i yourfastafile.fa \n\noptional: \n-o output.html"
help = help+"\n\nPS: In your fasta file first 50 characters of each header must be unique!\n"



if '-h' in sys.argv:
        print help+"\n"
        sys.exit()

if '-i' in sys.argv:
        inputfile=sys.argv[sys.argv.index('-i')+1]
        seq_dic, seq_list = co.fastareader(inputfile)
else:
        print "No input\n"
        sys.exit()

if '-o' in sys.argv:
    outputfile =sys.argv[sys.argv.index('-o')+1]
else:
	outputfile = inputfile[:(inputfile.rindex('.')-len(inputfile))]+'.html'


if '--print' in sys.argv:
	printsign=1
else:
	printsign=0		

if '-newton' in sys.argv:
	newton=1
	ID=sys.argv[sys.argv.index('-newton')+1]
	jobID=sys.argv[sys.argv.index('-newton')+2]
else:
	newton=0
	
if '-param' in sys.argv:
	param=1
	parameterfile=sys.argv[sys.argv.index('-param')+1]
else:
	param=0
	
def cprint(thetexttobeprinted):
	if printsign:
		print thetexttobeprinted
		return
	else:
		return
	
def table2features(table_in):
	table=open(table_in,'r')
	lines=table.readlines()
	
	header=lines[0].replace('\n','')
	fullsequence=lines[1].replace('\n','')
	TMinfo=lines[2].replace('\n','')
	del lines[0]
	del lines[0]
	del lines[0]
	tag=co.removesymbols(header)[:50]
	
	seqlength=len(fullsequence)
	
	features='\''+tag+'\''+','+'\''+str(fullsequence)+'\''+','+'\''+str(TMinfo)+'\''+','
	domains=''
	intervals=''
	scores=''
	databases=''
	coverages=''
	sequences=''
	alignments=''
	
	
	for line in lines:
		if line:
			if not 'XXXX' in line.split('\t')[0]: 
				
				domains=domains+line.split('\t')[0]+';'
				intervals=intervals+line.split('\t')[1]+';'
				scores=scores+line.split('\t')[2]+';'
				databases=databases+line.split('\t')[3]+';'
				sequences=sequences+line.split('\t')[5].replace("\n","")+';'
				alignments=alignments+line.split('\t')[6].replace("\n","").replace(" ","_")+';'
				
				
				cov=line.split('\t')[4].split('|')[0].strip()+'_'+line.split('\t')[4].split('|')[1].strip()+'_'+line.split('\t')[4].split('|')[2].strip()
				
				cprint (cov+'\n')
				coverages=coverages+cov+';'
			
		
	features=features+'\''+domains+'\''+','+'\''+intervals+'\''+','+'\''+scores+'\''+','+'\''+databases+'\''+','+'\''+coverages+'\''+','+'\''+sequences+'\''+','+'\''+alignments+'\''
	cprint (coverages)
	#print features
	return features	




def htmlprint1():
	return '''
<html>
	<head>
		<LINK REL="SHORTCUT ICON"  HREF="http://web.utk.edu/~oadebali/CoDAT.ico">
		<title>Domain Architecures</title>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
		<style>
		#header
			{
			top: 0;
			display:block;
			background: white;
			}
		#header.fixed
			{
			position:fixed; 
			top: 0;  /*fixing it at the top*/
			z-index: 999;  /* over any other element*/
			}	

		</style>
		<SCRIPT LANGUAGE="JavaScript">
		</SCRIPT>
			<script src="http://web.utk.edu/~oadebali/architecturesvg.js"></script>
			<script src="http://web.utk.edu/~oadebali/jquery-ui.js"></script>
	</head>
	 
	 <body link="black">
	
			<table id="header" border="0">
					<tr>
				<td><img src="http://web.utk.edu/~oadebali/codost.png" width="100px" height="auto"></td>

<!-- 				<td id='tablecolorcode' rowspan="2">
				<svg id='colorcode' width="360px" height="80px"><script>drawcolorcode()</script></svg>
				</td> -->

				
								<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".tm").show()'><font size="1"/><b/>Show TM</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".coverage").show()'><font size="1"/><b/>Show Coverage</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".alignment").show()'><font size="1"/><b/>Show Alignment</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".sequence").show()'><font size="1"/><b/>Show Sequence</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".mainarch").hide()'><font size="1"/><b/>Hide Domains</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".sequence").hide();$(".coverage").hide();$(".alignment").hide();$(".tm").hide();$(".mainarch").show();'><font size="1"/><b/>Reset</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='$(".play").show()'><font size="1"/><b/>Play</button></td>
				<td width="75" align="center" valign="center"><a href="detailed_output.txt" STYLE="TEXT-DECORATION: NONE"><font color="black" size="2"><b/>log</font></a></h2></td>
		
			</tr>
			<tr class="play">
				<td></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='drag()'><font size="1"/><b/>Drag</button></td>
				<td><button  style="position: relative; left: 0px; top: 0px; width: 115px; height: 20px;" onclick='dragfix()'><font size="1"/><b/>Fix</button></td><!-- <td><button  style="position: relative; left: 0px; top: 0px; width: 117px; height: 20px;" onclick='$("#tablecolorcode").show()'><font size="1"/><b/>Show ColorCode</button></td> -->
				<td width="75" align="center" valign="center"><a href="#" onClick=" window.print(); return false" STYLE="TEXT-DECORATION: NONE"><font color="black" size="2"><b/>PDF</font></a></td>
			</tr>

		</table>
		<script> //by default, the static menu is hidden
  var showStaticMenuBar = false;

  //when scrolling...
  $(window).scroll(function () {

      //if the static menu is not yet visible...
      if (showStaticMenuBar == false) {
          //if I scroll more than 200px, I show it 
          if ($(window).scrollTop() >= 200) {
              //showing the static menu
              $('#header').addClass('fixed');

              showStaticMenuBar = true;

          }

      }
      //if the static menu is already visible...
      else {
          if ($(window).scrollTop() < 200) {
              $('#header').removeClass('fixed');

              //I define it as hidden
              showStaticMenuBar = false;
          }
      }
  });
  </script>
		<table border="0", style='table-layout:fixed'>
	'''

def htmlprint2(canvaslength,features,tablename,proteincount):
	list=features.split(",")
	tag=list[0]
	del list[0]
	
	text= '''<tr>'''
	
	if newton:
		text=text+'<td><a href=CODOST_'+ID+'.o'+jobID+'.'+str(proteincount)+'>o</a></td>'
		text=text+'<td><a href=CODOST_'+ID+'.e'+jobID+'.'+str(proteincount)+'>e</a></td>'
	else:
		text=text+'<td></td><td></td>'
				
				
	text=text+'''
<td><a href="JavaScript:newpopup('
'''
	text=text+tablename
	text=text+'''',536,954);">'''
	text=text+tag		
	text=text+'''</a></td>
	
	<td class="draggable" width="'''+str(canvaslength)+'''">
	
	<svg class='backbone' id='''
	text=text+tag	
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawbackbone('''
	text=text+features
	text=text+''')</script>

	<svg class='tm' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">			
	<script>drawTM('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	<svg class='mainarch' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">			
	<script>drawarch('''
	text=text+features
	text=text+''')</script>
	</svg>
				
	<svg class='alignment' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawalignment('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	<svg class='sequence' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawsequence('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	<svg class='coverage' id='''
	text=text+tag
	text=text+''' baseProfile="full" height="40" version="1.1" width="'''+str(canvaslength)+'''" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<script>drawcoverage('''
	text=text+features
	text=text+''')</script>
	</svg>
	
	</svg>					
	</td>			
	</tr>
	'''
	return text
	
def htmlprint3():
	return '''
	<script>
	$(".coverage").hide();
	$(".alignment").hide();
	$(".tm").hide();
	$(".sequence").hide();
	$(".play").hide();
	</script>
		</table>
		'''
def htmlprintparameterline():
	pfile=open(parameterfile,'r')
	text='<p style="color:grey">'+pfile.readlines()[0]+'</p>'
	return text
	
def htmlprintEND():
	return '''
	</body>
</html>
		'''
		
		
cprint(outputfile)
out=open(outputfile,'w')		
out.write(htmlprint1())

proteincount=1
for proteinheader in seq_list:
	header=co.removesymbols(proteinheader)[0:50]
	filename=header+'.table'
	if newton:
		filename=ID+filename
	features=table2features(filename)
	canvaslength=len(seq_dic[proteinheader])+5
	out.write(htmlprint2(canvaslength,features,filename,proteincount))
	proteincount=proteincount+1
out.write(htmlprint3())

if param:
	out.write(htmlprintparameterline())

out.write(htmlprintEND())


out.close()

timeend=datetime.datetime.now()

timepassed=timeend-timestart

cprint("Time spent:")
cprint(str(timepassed))



